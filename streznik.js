var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	var user = req.query.uporabniskoIme;
	var pass = req.query.geslo;
	
	if (preveriSpomin(user, pass) || preveriDatotekaStreznik(user, pass)) {
		res.send({status: "true", napaka: ""});
	}
	else if (!(preveriSpomin(user, pass) || preveriDatotekaStreznik(user, pass))) {
		res.send({status: "false", napaka: "Avtentikacija ni uspela."});
	}
	else if (user==null || pass==null || user.length==0 || pass.length==0) {
		res.send({status: "false", napaka: "Napačna zahteva."});
	}
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	var user = req.query.uporabniskoIme;
	var pass = req.query.geslo;
	
	if (preveriSpomin(user, pass) || preveriDatotekaStreznik(user, pass)) {
		res.send("<html><title>Uspešno</title></html>");
		res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>" + user + "</b> uspešno prijavljen v sistem!</p></body></html>");
	}
	else {
		res.send("<html><title>Napaka</title></html>");
		res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>" + user + "</b> nima pravice prijave v sistem!</p></body></html>");
	}
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var podatkiDatotekaStreznik = {};
var file = fs.readFileSync(_dirname + "/public/podatki/uporabniki_streznik.json").toString();
podatkiDatotekaStreznik = JSON.parse(file);


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	var userPass = {};
	for (var i in podatkiSpomin) {
		userPass = podatkiSpomin[i].split("/");
		
		if (userPass[0]==uporabniskoIme && userPass[1]==geslo) {
			return true;
		}
	}
	return false;
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	var user, pass;
	for (var i in podatkiDatotekaStreznik) {
		user = podatkiDatotekaStreznik[i]["uporabnik"];
		pass = podatkiDatotekaStreznik[i]["geslo"];
		
		if(user==uporabniskoIme && pass==geslo) {
			return true;
		}
	}
	return false;
}